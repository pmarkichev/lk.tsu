﻿using AutoMapper;
using Infrastructure.Models;

namespace lk.tsu.Mapping
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<Student, StudentDTO>().ReverseMap();
        }
    }
}