import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {UsersService} from '../../services/UserAccounts/users.service';
import {LoginModel} from '../../../models/login-model';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  userAc: LoginModel = {} as LoginModel;
  constructor(private router: Router, private http: HttpClient, private userService: UsersService){
  }
  loginForm = new FormGroup({
    login: new FormControl(''),
    password: new FormControl('')
  });
  ngOnInit(): void {
  }
  SetTokenStudent(): void {
    this.userService.SetTokenStudent(this.userAc)
      .subscribe((res) => {
        if (res && JSON.stringify(res)) {
          localStorage.setItem('user', JSON.stringify(res));
          this.userAc.token = localStorage.getItem('user');
          this.router.navigate(['personal-info']);
        } else {
          localStorage.setItem('access_token', null);
        }
      });
  }
}
