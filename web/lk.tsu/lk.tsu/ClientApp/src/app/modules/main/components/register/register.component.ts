import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {RegisterModel} from '../../../models/RegisterModel';
import {UsersService} from '../../services/UserAccounts/users.service';
import {ConfirmPasswordValidator} from '../../directives/confirm-password-validator.directive';
import {StudentsService} from '../../services/Students/students.service';
import {Student} from '../../../models/student';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router, private http: HttpClient, private userService: UsersService) {
  }
  userAc: RegisterModel = {} as RegisterModel;
  RegForm = new FormGroup({
    loginName: new FormControl(''),
    userPassword: new FormControl(''),
    confirm_password: new FormControl(''),
  }, { validators: ConfirmPasswordValidator });
  ngOnInit(): void {
  }
  setIdStudent(): void {
    this.userService.setIdStudent(this.userAc)
      .subscribe(response => this.userAc = response);
  }

}
