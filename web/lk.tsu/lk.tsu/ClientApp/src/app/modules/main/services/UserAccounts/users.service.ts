import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {RegisterModel} from '../../../models/RegisterModel';
import {Observable} from 'rxjs';
import {LoginModel} from '../../../models/login-model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private readonly scope = 'openid offline_access';
  private readonly grantType = 'password';
  regUrl = 'http://localhost:5000/account/register';
  authUrl = 'http://localhost:5000/account/token';
  constructor(private http: HttpClient) {
  }

  setIdStudent(user: RegisterModel): Observable<any> {
    return this.http.post<any>(this.regUrl, user);
  }
  SetTokenStudent(user: LoginModel): Observable<any>{
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const body = {...user, grant_type: this.grantType, scope: this.scope };
    let params = new HttpParams();
    for (const [key, value] of Object.entries(body)) {
      params = params.set(key, value);
    }
    return this.http.post<LoginModel>(this.authUrl, params, { headers });
  }
}
