import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

export const ConfirmPasswordValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const password = control.get('userPassword');
    const confirmPassword = control.get('confirm_password');

    return (password.value === confirmPassword.value) ? null : { identityRevealed: true };
  };

