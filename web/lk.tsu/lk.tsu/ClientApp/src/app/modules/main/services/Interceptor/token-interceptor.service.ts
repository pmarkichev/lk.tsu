import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userToken = JSON.parse(localStorage.getItem('user') as any);
    let modifiedReq = req;
    if (userToken !== null) {
      modifiedReq = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + userToken.access_token),
      });
    }
    return next.handle(modifiedReq);
  }
}
