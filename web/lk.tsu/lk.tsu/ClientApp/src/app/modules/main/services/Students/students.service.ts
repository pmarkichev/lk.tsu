import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Student} from '../../../models/student';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {
  constructor(private http: HttpClient) {
  }

  apiUrl = 'http://localhost:5000/student/';
  updateStudent(student: Student, userId: string): Observable<Student> {
    const options = {params: new HttpParams().set('userId', userId)};
    return this.http.put<Student>(this.apiUrl, student, options);
  }
  getStudent(): Observable<any> {
    return this.http.get<any>(this.apiUrl + 'user');
  }

}
