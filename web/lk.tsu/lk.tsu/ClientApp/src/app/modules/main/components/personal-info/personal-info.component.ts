import {Component, OnInit} from '@angular/core';
import {Student} from '../../../models/student';
import {HttpClient} from '@angular/common/http';
import {StudentsService} from '../../services/Students/students.service';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss']
})

export class PersonalInfoComponent implements OnInit {

  student: Student = {} as Student;
  userid: string;

  constructor(private http: HttpClient, private studentService: StudentsService) {
  }


  ngOnInit(): void {
  }
  updateStudent(): void {
  this.studentService.updateStudent(this.student, this.userid).subscribe(response => this.student = response);
  }
  getStudents(): void {
    this.studentService.getStudent()
      .subscribe(response => this.student = response);
  }


}
