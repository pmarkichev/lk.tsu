import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DemoComponent} from './components/demo/demo.component';
import {RouterModule, Routes} from '@angular/router';
import {PersonalInfoComponent} from './components/personal-info/personal-info.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import {TimetableComponent} from './components/timetable/timetable.component';
import {BrowserModule} from '@angular/platform-browser';
import {StudentsService} from './services/Students/students.service';
import { AuthComponent } from './components/auth/auth.component';
import { RegisterComponent } from './components/register/register.component';
import {UserGuardGuard} from './guards/user-guard.guard';
import {UsersService} from './services/UserAccounts/users.service';

const routes: Routes = [
  {
    path: 'demo',
    data: {
      title: 'Demo'
    },
    component: DemoComponent,
    canActivate: [UserGuardGuard]
  },
  {
    path: 'personal-info',
    data: {
      title: 'LK'
    },
    component: PersonalInfoComponent,
    canActivate: [UserGuardGuard]
  },
  {
    path: 'timetable',
    data: {
      title: 'Timetable'
    },
    component: TimetableComponent,
    canActivate: [UserGuardGuard]
  },
  {
    path: 'auth',
    data: {
      title: 'Authorization'
    },
    component: AuthComponent
  },
  {
    path: 'register',
    data: {
      title: 'Registration'
    },
    component: RegisterComponent
  },
];

@NgModule({
  declarations: [
    PersonalInfoComponent,
    DemoComponent,
    TimetableComponent,
    AuthComponent,
    RegisterComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    FormsModule,
    BrowserModule,
    MatButtonModule,
  ],
  providers: [StudentsService,
    UserGuardGuard, UsersService],
  exports: [
    RouterModule
  ],
})
export class MainModule {
}

