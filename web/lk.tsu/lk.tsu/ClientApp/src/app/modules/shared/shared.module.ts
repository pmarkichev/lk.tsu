import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopMenuComponent } from './components/top-menu/top-menu.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MainModule} from '../main/main.module';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [TopMenuComponent],
  exports: [
    TopMenuComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MainModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
