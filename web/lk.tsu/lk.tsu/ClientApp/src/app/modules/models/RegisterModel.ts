export interface RegisterModel {
  loginName: string;
  userPassword: string;
  confirmPassword: string;
}
