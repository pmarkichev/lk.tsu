export interface Student {
  surname: string;
  name: string;
  profile: Profiles;
  gender: Gender;
  bd: Date;
  hcountry: Hcountry;
  facult: Facult;
  lglob: Lglob;
  area: Area;
  phone: string;
  mphone: string;
  ext: string;
  email: string;
  skype: string;
  stlern: Date;
}

export enum Profiles {
  Student,
  Prep
}

export enum Gender {
  Male,
  Female
}

export enum Facult {
  VT,
  IPY
}

export enum Lglob {
  Tula,
  Kimovsk
}

export enum Area {
  Global,
  Local
}

export enum Hcountry {
  Russian,
  USA
}
