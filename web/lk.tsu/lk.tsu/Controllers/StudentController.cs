﻿using System.Linq;
using AutoMapper;
using Infrastructure;
using Infrastructure.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OpenIddict.Validation.AspNetCore;
namespace lk.tsu.Controllers
{
    [ApiController]
    public class StudentController : ControllerBase
    {

        private readonly ILogger<StudentController> _logger;
        private readonly LkTsuContext _LkTsuContext;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string _getUserName;
        public StudentController(ILogger<StudentController> logger, LkTsuContext lkTsuContext, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            _mapper = mapper;
            _LkTsuContext = lkTsuContext;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
            _getUserName = _httpContextAccessor.HttpContext.User.Identity.Name;
        }

        [Route("~/student/user")]
        [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
        [HttpGet]
        public Student Get()
        {
            return _LkTsuContext.Student.First(s => s.UserName == _getUserName);
        }
        [Route("~/student")]
        [Authorize(AuthenticationSchemes = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme)]
        [HttpPut]
        public StudentDTO UpdateStudent(StudentDTO studentDto)
        {
            var entry = _LkTsuContext.Student.First(s => s.UserName == _getUserName);
            if (entry != null)
            {
                _mapper.Map(studentDto, entry);
                _LkTsuContext.Update(entry);
                _LkTsuContext.SaveChanges();
            }
            return studentDto;
        }
    }
}