﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Primitives;
using Infrastructure;
using Infrastructure.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OpenIddict.Abstractions;
using Microsoft.Extensions.Options;
using OpenIddict.Server.AspNetCore;

namespace lk.tsu.Controllers
{
    public class AccountController : ControllerBase
    {
        private UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly LkTsuContext _LkTsuContext;
        private readonly IOptions<IdentityOptions> _identityOptions;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager,
            LkTsuContext lkTsuContext, IOptions<IdentityOptions> identityOptions)
        {
            _LkTsuContext = lkTsuContext;
            _userManager = userManager;
            _signInManager = signInManager;
            _identityOptions = identityOptions;
        }

        [HttpPost("~/account/register")]
        public async Task<IActionResult> Register([FromBody] RegisterUser regUser)
        {
            var userIsExist = (await _userManager.FindByNameAsync(regUser.LoginName)) != null;
            if (userIsExist)
            {
                return BadRequest(new
                {
                    ErrorDescription = "Такой пользователь уже существует"
                });
            }

            var user = new User {UserName = regUser.LoginName};
            var result = await _userManager.CreateAsync(user, regUser.UserPassword);

            if (result.Succeeded)
            {
                await _signInManager.SignInAsync(user, false);
                Student set_id_student = new Student{UserName = user.UserName};
                _LkTsuContext.Student.Add(set_id_student);
                _LkTsuContext.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest(new
                {
                    ErrorDescription = "Произошла неизвестная ошибка"
                });
            }
        }

        private async Task<IActionResult> SignIn(OpenIddictRequest request)
        {
            var user = new User()
            {
                Id = Guid.NewGuid().ToString(),
                UserName = request.Username,
                NormalizedUserName = request.Username,
            };


            var ticket = await CreateTicketAsync(request, user);
            return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
        }

        private async Task<AuthenticationTicket> CreateTicketAsync(OpenIddictRequest request, User User,
            AuthenticationProperties properties = null)
        {
            var principal = await _signInManager.CreateUserPrincipalAsync(User);

            principal.SetScopes(new[]
            {
                OpenIddictConstants.Scopes.OpenId,
                OpenIddictConstants.Scopes.Email,
                OpenIddictConstants.Scopes.Profile,
                OpenIddictConstants.Scopes.OfflineAccess,
                OpenIddictConstants.Scopes.Roles
            }.Intersect(request.GetScopes()));
            var ticket = new AuthenticationTicket(principal, properties ?? new AuthenticationProperties(),
                OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
            var claims = ticket.Principal.Claims
                .Where(item => item.Type != _identityOptions.Value.ClaimsIdentity.SecurityStampClaimType)
                .ToList();
            foreach (var claim in claims)
            {
                claim.SetDestinations(OpenIddictConstants.Destinations.AccessToken,
                    OpenIddictConstants.Destinations.IdentityToken);
            }
            var identity = principal.Identity as ClaimsIdentity;
             identity.AddClaim(CustomClaimTypes.Fullname, User.UserName,
                 OpenIddictConstants.Destinations.IdentityToken);
             identity.AddClaim(CustomClaimTypes.Username, User.UserName,
                 OpenIddictConstants.Destinations.IdentityToken);
             identity.AddClaim(ClaimTypes.NameIdentifier, User.Id, ClaimValueTypes.String);

            return ticket;
        }


        [HttpPost("~/account/token"), Produces("application/json")]
        public async Task<IActionResult> Token()
        {
            var request = HttpContext.GetOpenIddictServerRequest();
            
            if (request.IsPasswordGrantType())
            {
                var user = await _userManager.FindByNameAsync(request.Username);

                if (user == null)
                {
                    return BadRequest(new
                    {
                        ErrorDescription = "Данного пользователя не существует"
                    });
                }

                var result = await _userManager.CheckPasswordAsync(user, request.Password);
                if (result)
                {
                    return await SignIn(request);
                }
                else
                {
                    return BadRequest(new OpenIdConnectResponse
                    {
                        Error = OpenIdConnectConstants.Errors.InvalidGrant,
                        ErrorDescription = "Неправильные логин/пароль"
                    });
                }
            }

            if (request.IsRefreshTokenGrantType())
            {
                var info = await HttpContext.AuthenticateAsync(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
                var user = await _userManager.GetUserAsync(info.Principal);
                if (user != null)
                {
                    return await SignIn(request);
                }
                else
                {
                    return BadRequest(new OpenIdConnectResponse
                    {
                        Error = OpenIdConnectConstants.Errors.InvalidGrant,
                        ErrorDescription = "The refresh token is no longer valid."
                    });
                }
            }

            return BadRequest(new OpenIdConnectResponse
            {
                Error = OpenIdConnectConstants.Errors.UnsupportedGrantType,
                ErrorDescription = "The specified grant type is not supported"
            });
        }

        public static class CustomClaimTypes
        {
            public const string Fullname = "fullname";
            public const string Username = "username";
        }
        
    }
}