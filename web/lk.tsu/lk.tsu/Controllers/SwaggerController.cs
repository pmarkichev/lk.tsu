﻿using Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace lk.tsu.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class SwaggerController: ControllerBase
    {
        private readonly LkTsuContext _lkTsuContext;
    }
}