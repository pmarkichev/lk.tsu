using System;

namespace Infrastructure.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public Profiles Profile { get; set; }
        public Gender Gender { get; set; }
        public DateTime Bd { get; set; }
        public Hcountry Hcountry { get; set; }
        public Facult Facult { get; set; }
        public Lglob Lglob { get; set; }
        public Area Area { get; set; }
        public string Phone { get; set; }
        public string Mphone { get; set; }
        public string Ext { get; set; }
        public string Email { get; set; }
        public string Skype { get; set; }
        public DateTime Stlern { get; set; }
    }
    public enum Profiles
    {
        Student = 0,
        Prep = 1
    }
    public enum Gender
    {
        Male = 0,
        Female = 1
    }
    public enum Facult
    {
        VT = 0,
        IPY = 1
    }
    public enum Lglob
    {
        Tula = 0,
        Kimovsk = 1
    }
    public enum Area
    {
        Global = 0,
        Local = 1
    }
    public enum Hcountry
    {
        Russian = 0,
        USA = 1
    }

}
