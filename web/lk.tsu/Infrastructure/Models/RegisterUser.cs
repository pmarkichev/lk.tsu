﻿namespace Infrastructure.Models
{
    public class RegisterUser
    {
        public string LoginName { get; set; }
        public string UserPassword { get; set; }
    }
}