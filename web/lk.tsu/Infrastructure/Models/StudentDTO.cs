﻿using System;

namespace Infrastructure.Models
{
    public class StudentDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public Profiles Profile { get; set; }
        public Gender Gender { get; set; }
        public DateTime Bd { get; set; }
        public Hcountry Hcountry { get; set; }
        public Facult Facult { get; set; }
        public Lglob Lglob { get; set; }
        public Area Area { get; set; }
        public string Phone { get; set; }
        public string Mphone { get; set; }
        public string Ext { get; set; }
        public string Email { get; set; }
        public string Skype { get; set; }
        public DateTime Stlern { get; set; }
    }
}