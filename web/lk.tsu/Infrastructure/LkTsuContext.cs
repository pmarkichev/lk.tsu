﻿using Infrastructure.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class LkTsuContext : IdentityDbContext<User, Role, string>
    {

        public LkTsuContext(DbContextOptions<LkTsuContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<Student> Student { get; set; }
    }
}